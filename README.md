# Epistemic Task Analysis

Epistemic Task Analysis is a unified qualitative/quantitative approach to study the interplay between human behavior, contextual and environmental stimuli, and mental processes.